import React from 'react';
import {KeyboardAvoidingView} from 'react-native';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
  SafeAreaView,
} from 'react-native';

class Main extends React.Component {
  static navigationOptions = {
    title: 'Susirasom!',
  };



  state = {
    name: '',
  };

  onPress = () =>
    this.props.navigation.navigate('Chat', { name: this.state.name });

  onChangeText = name => this.setState({ name });

  render() {
    return (
		<KeyboardAvoidingView behavior="padding" style={styles.form}>
      <View style={styles.container2}>
        <Text style={styles.title}>Slapyvardis</Text>
        <TextInput
		  underlineColorAndroid='transparent'
          style={styles.nameInput}
          placeHolder="Vardenis"
          onChangeText={this.onChangeText}
          value={this.state.name}
        />
        <TouchableOpacity style={styles.touchableopacity} 
			onPress={this.onPress}>
          <Text style={styles.buttonText}>Prisijungti</Text>
        </TouchableOpacity>
      </View>
	</KeyboardAvoidingView>
    );
  }
}

const offset = 24;

const styles = StyleSheet.create({
  title: {
    marginTop: offset * 4,
    fontSize: 30,
	color: '#00C1F2',
  },
  nameInput: {
    margin: offset,
    paddingHorizontal: offset,
    borderColor: '#111111',
	backgroundColor:'#00C1F2',
    borderWidth: 2,
	width: offset * 14,
	height: offset * 2,
	borderRadius: 20,
	fontSize: 25,
	textAlign: 'center',
	  
  },
  buttonText: {
	  fontSize: 20,
  },
	touchableopacity: {
    	marginTop:1,
    	paddingTop:15,
		paddingBottom:15,
    	marginLeft:30,
		marginRight:30,
		backgroundColor:'#00C1F2',
		borderRadius:20,
		borderWidth: 2,
		borderColor: '#000000',		
	},
	container: {
		flex: 1,
	},
	container2: {
    	alignItems: 'center',
	},
	form: {
    flex: 1,
    justifyContent: 'space-between',
	}
});

export default Main;
